<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->group(function () {
    Route::post('register',                   'AuthController@register');
    Route::post('login',                      'AuthController@login');

    Route::get('categories',                  'CategoryController@index');
    Route::get('categories/show/{id}',        'CategoryController@show');

    Route::get('products',                    'ProductController@index');
    Route::get('products/show/{id}',          'ProductController@show');

    Route::get('category/{id}/products',      'CategoryProductsController@show');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('logout',                   'AuthController@logout');

        Route::get('user',                      'UserController@show');
        Route::post('user/update',              'UserController@update');

        Route::post('category/store',           'CategoryController@store');
        Route::post('category/update/{id}',     'CategoryController@update');
        Route::delete('category/destroy/{id}',  'CategoryController@destroy');

        Route::post('product/store',            'ProductController@store');
        Route::post('product/update/{id}',      'ProductController@update');
        Route::delete('product/destroy/{id}',   'ProductController@destroy');
    });
});
