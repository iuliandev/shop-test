## Clone the repository

git clone https://iuliandev@bitbucket.org/iuliandev/shop-test.git


## Switch to the repo folder

cd shop-test


## Install all the dependencies using composer

composer install


## Copy the example env file and make the required configuration changes in the .env file

cp .env.example .env


## Connect to your database

DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=


## Run the database migrations

php artisan migrate


## Insert demo data in DB running seeds

php artisan db:seed


## Start the local development server

php artisan serve
