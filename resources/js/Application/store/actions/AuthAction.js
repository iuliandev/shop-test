import { SignUpService, LoginService } from '../services/AuthService';

export const signUp = (credentials) => {
  return (dispatch) => {
    if(credentials.password.length < 6) {
      return dispatch({type: 'SHORT_PASSWORD'});
    }

    SignUpService(credentials).then((res) => {
      if(res.data.access_token !== null) {
        localStorage.setItem('user', 'Bearer ' + res.data.access_token);
        dispatch({type: 'SIGNUP_SUCCESS'});
        history.push('/dashboard');
      } else {
        dispatch({type: 'SIGNUP_ERROR', res});
      }
    },
    error => {
      dispatch({type: 'SIGNUP_ERROR', error});
    });
  }
}

export const LoginUser = (credentials, history) => {
  return (dispatch) => {
    if(credentials.password.length < 6) {
      return dispatch({type: 'SHORT_PASSWORD'});
    }

    LoginService(credentials).then((res) => {
      console.log('res', res);
      if(res.data && res.data.access_token !== null) {
        localStorage.setItem('user', 'Bearer ' + res.data.access_token);
        dispatch({type: 'LOGIN_SUCCESS'});
        history.push('/dashboard');
      } else {
        dispatch({type: 'LOGIN_ERROR', res});
      }
    },
    error => {
      dispatch({type: 'CODE_ERROR', error});
      console.log('error', error);
    });
  }
}
