const initState = {
  authResponse: null
};

const AuthReducer = (state = initState, action) => {
  switch(action.type) {
    case 'LOGIN_SUCCESS':
      console.log(action);
      return {...state, authResponse: 'redirecting to dashboard'};
    case 'LOGIN_ERROR':
      console.log(action);
      return {...state, authResponse: action.res.message};
    case 'SHORT_PASSWORD':
      console.log(action);
      return {...state, authResponse: 'Password is too short'};
    case 'SIGNUP_SUCCESS':
      console.log(action);
      return {...state, authResponse: 'Sign up was successfully done'};
    case 'SIGNUP_ERROR':
      console.log(action);
      return {...state, authResponse: action.res.message};
    case 'CODE_ERROR':
      console.log(action);
      return {...state, authResponse: 'There seems to be a problem please try again later'};
    default:
      return state;
  }
}

export default AuthReducer;
