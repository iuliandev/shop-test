import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import CategoryReducer from './CategoryReducer';
import ProductReducer from './ProductReducer';


const RootReducer = combineReducers({
    auth: AuthReducer,
    categry: CategoryReducer,
    product: ProductReducer,
});

export default RootReducer;
