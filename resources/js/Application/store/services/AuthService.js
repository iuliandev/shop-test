import HttpService from './HttpService';

export const SignUpService = (credentials) => {
  const http = new HttpService();
  const signUpUrl = "register";

  return http.postData(credentials, signUpUrl).then(data => {
    return data;
  }).catch(error => console.log(error));
}

export const LoginService = (credentials, propsHistory) => {
  const http = new HttpService();
  const signUpUrl = "login";

  return http.postData(credentials, signUpUrl).then(data => {
    console.log('data', data);
    return data;
  }).catch(error => console.log(error));
}
