import React from 'react';
import {Switch, Route} from 'react-router-dom';

import Slider from './Section/Slider';
import ChooseUs from './Section/ChooseUs';
import Service from './Section/Service';
import Products from './Section/Products';
import Testimonials from './Section/Testimonials';

const Home = (props) => {
  return (
    <>
      <Slider />
      <ChooseUs />
      <Service />
      <Products />
      <Testimonials />
    </>
  );
}

export default Home;
