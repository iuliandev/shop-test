import React from 'react';

const Service = () => {
  return (
    <section className="service">
      <div className="container">
        <div className="row">
          <div className="col-md-8 offset-md-2">
            <div className="title">
              <h2>Service <strong className="black">Process</strong></h2>
              <span>Easy and effective way to get your device repair</span>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service1.png"/></i>
              <h3>Fast service</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service2.png"/></i>
              <h3>Secure payments</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service3.png"/></i>
              <h3>Expert team</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service4.png"/></i>
              <h3>Affordable services</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service5.png"/></i>
              <h3>90 Days warranty</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
          <div className="col-xl-4 col-lg-4 col-md-4 col-sm-12">
            <div className="service-box">
              <i><img src="lighten/icon/service6.png"/></i>
              <h3>Award winning</h3>
              <p>Exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Service;
