import React from 'react';

const Products = () => {
  return (
    <section className="product">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="title">
              <h2>our <strong className="black">products</strong></h2>
              <span>We package the products with best services to make you a happy customer.</span>
            </div>
          </div>
        </div>
      </div>
      <div className="product-bg-white">
        <div className="container">
          <div className="row">
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p1.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p2.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p3.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p4.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p5.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p2.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p6.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
            <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12">
              <div className="product-box">
                <i><img src="lighten/icon/p7.png"/></i>
                <h3>Norton Internet Security</h3>
                <span>$25.00</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Products;
