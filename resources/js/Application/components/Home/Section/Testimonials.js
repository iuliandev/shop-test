import React from 'react';

const Testimonials = () => {
  return (
    <section className="testimonials">
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <div className="title">
              <h3>What Clients Say?</h3>
            </div>
          </div>
        </div>
        <div id="client_slider" className="carousel slide banner_Client" data-ride="carousel">
          <ol className="carousel-indicators">
            <li data-target="#client_slider" data-slide-to="0" className="active"></li>
            <li data-target="#client_slider" data-slide-to="1"></li>
            <li data-target="#client_slider" data-slide-to="2"></li>
          </ol>
          <div className="carousel-inner">
            <div className="carousel-item active">
              <div className="container">
                <div className="carousel-caption text-bg">
                  <div className="img_bg">
                    <i><img src="lighten/images/lllll.png"/><span>Jone Due<br /><strong className="date">12/02/2019</strong></span></i>
                  </div>
                  <p>You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am really satisfied with my first laptop service.<br />
                    You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am
                  </p>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <div className="container">
                <div className="carousel-caption text-bg">
                  <div className="img_bg">
                    <i><img src="lighten/images/lllll.png"/><span>Jone Due<br /><strong className="date">12/02/2019</strong></span></i>
                  </div>
                  <p>You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am really satisfied with my first laptop service.<br />
                    You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am
                  </p>
                </div>
              </div>
            </div>
            <div className="carousel-item">
              <div className="container">
                <div className="carousel-caption text-bg">
                  <div className="img_bg">
                    <i><img src="lighten/images/lllll.png"/><span>Jone Due<br /><strong className="date">12/02/2019</strong></span></i>
                  </div>
                  <p>You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am really satisfied with my first laptop service.<br />
                    You guys rock! Thank you for making it painless, pleasant and most of all hassle free! I wish I would have thought of it first. I am
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Testimonials;
