import React from 'react';
import { Route, Switch } from "react-router-dom";
import Home from './Home/Home';
import Login from './Login/Login';
import Register from './Register/Register';
import Shop from './Shop/Shop';
import Blog from './Blog/Blog';
import Contact from './Contact/Contact';
import About from './About/About';
import Dashboard from './Dashboard/Dashboard';
import {PrivateRoute} from './PrivateRoute';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/home" component={Home}/>
      <Route exact path="/login" component={Login}/>
      <Route exact path="/register" component={Register} />
      <Route exact path="/products" component={Shop} />
      <Route exact path="/blog" component={Blog} />
      <Route exact path="/contact" component={Contact} />
      <Route exact path="/about" component={About} />
      <PrivateRoute path="/dashboard" component={Dashboard} />
    </Switch>
  );
}

export default Routes;
