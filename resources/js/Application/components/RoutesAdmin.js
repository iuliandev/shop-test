import React from 'react';
import { Route, Switch } from "react-router-dom";
import Dashboard from './Dashboard/Dashboard';
import {PrivateRoute} from './PrivateRoute';


const RoutesAdmin = () => {
  return (
    <Switch>
      <PrivateRoute path="/dashboard" component={Dashboard} />
    </Switch>
  );
}

export default RoutesAdmin;
