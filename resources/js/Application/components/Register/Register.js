import React, { Component } from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { signUp } from '../../store/actions/AuthAction';

// Build this first as a Class style.
// Scope is to learn both methods
// TODO: change into function style using "useState"
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      password_confirm: ''
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signUp(this.state);
  }

  handleChange = (e) => {
    this.setState({
      [e.target.id] : e.target.value
    })
  }

  render() {
    const {authResponse} = this.props;

    return (
      <section className="register">
        <div className="brand_color">
          <Container maxWidth="md">
            <div className="row">
              <div className="col-md-12">
                <div className="titlepage">
                  <h2>Register</h2>
                </div>
              </div>
            </div>
          </Container>
        </div>
        <Container maxWidth="sm" style={{ marginTop: 40, marginBottom: 40 }}>
          <form validate="true" autoComplete="off" onSubmit={this.handleSubmit}>
            <TextField
              id="name"
              label="Name"
              style={{ marginBottom: 5 }}
              placeholder="Enter your name"
              fullWidth
              margin="normal"
              type="text"
              variant="outlined"
              required
              onChange={this.handleChange}
              value={this.state.name || ''}
            />
            <TextField
              id="email"
              label="Email"
              style={{ marginBottom: 5 }}
              placeholder="Enter your email"
              fullWidth
              margin="normal"
              type="email"
              variant="outlined"
              required
              onChange={this.handleChange}
              value={this.state.email || ''}
            />
            <TextField
              id="password"
              label="Password"
              style={{ marginBottom: 5 }}
              placeholder="Enter your password"
              fullWidth
              margin="normal"
              type="password"
              variant="outlined"
              required
              onChange={this.handleChange}
              value={this.state.password || ''}
            />
            <TextField
              id="password_confirm"
              label="Password confirm"
              style={{ marginBottom: 5 }}
              placeholder="Confirm your password"
              fullWidth
              margin="normal"
              type="password"
              variant="outlined"
              required
              onChange={this.handleChange}
              value={this.state.password_confirm || ''}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              size="large"
              fullWidth
              style={{ marginTop: 15 }}
            >
              Register
            </Button>
            <br />
            <br />
            <br />
            <b>{authResponse || ''}</b>
          </form>
        </Container>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authResponse: state.auth.authError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signUp:(creds) => dispatch(signUp(creds))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
