import React, {Component} from 'react';
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import { LoginUser } from '../../store/actions/AuthAction';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  handleSubmit = (e) => {
    e.preventDefault();
    console.log('login');
    console.log(this.state);
    this.props.LoginUser(this.state, this.props.history);
  }

  handleChange = (e) => {
    e.preventDefault();
    this.setState({
      [e.target.id] : e.target.value
    });
  }

  render() {
    const {authResponse} = this.props;

    return (
      <section className="login">
        <div className="brand_color">
          <Container maxWidth="md">
            <div className="row">
              <div className="col-md-12">
                <div className="titlepage">
                  <h2>Login</h2>
                </div>
              </div>
            </div>
          </Container>
        </div>
        <Container maxWidth="sm" style={{ marginTop: 40, marginBottom: 40 }}>
          <form
            validate="true"
            autoComplete="off"
            onSubmit={this.handleSubmit}
          >
            <TextField
              id="email"
              label="Email"
              style={{ marginBottom: 5 }}
              placeholder="Enter your email"
              fullWidth
              margin="normal"
              type="email"
              variant="outlined"
              required
              onChange={this.handleChange}
            />
            <TextField
              id="password"
              label="Password"
              style={{ marginBottom: 5 }}
              placeholder="Enter your password"
              fullWidth
              margin="normal"
              type="password"
              variant="outlined"
              required
              onChange={this.handleChange}
            />
            <Button
              type="submit"
              variant="contained"
              color="primary"
              size="large"
              fullWidth
              style={{ marginTop: 15 }}
            >
              Login
            </Button>
            <br />
            <br />
            <br />
            <b>{authResponse || ''}</b>
          </form>
        </Container>
      </section>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    authResponse: state.auth.authError
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    LoginUser:(creds, history) => dispatch(LoginUser(creds, history))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
