import React from 'react';

import {Link} from 'react-router-dom';

const Header = () => {
  return (
    <header>
      <div className="header">
        <div className="head_top">
          <div className="container">
            <div className="row">
              <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div className="top-box">
                  <ul className="sociel_link">
                    <li> <a href="#"><i className="fa fa-facebook-f"></i></a></li>
                    <li> <a href="#"><i className="fa fa-twitter"></i></a></li>
                    <li> <a href="#"><i className="fa fa-instagram"></i></a></li>
                    <li> <a href="#"><i className="fa fa-linkedin"></i></a></li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div className="top-box">
                  <p>long established fact that a reader will be </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
              <div className="full">
                <div className="center-desk">
                  <div className="logo">
                    <Link to="/">
                      <img src="lighten/images/logo.jpg" alt="logo"/>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xl-7 col-lg-7 col-md-9 col-sm-9">
              <div className="menu-area">
                <div className="limit-box">
                  <nav className="main-menu">
                    <ul className="menu-area-main">
                      <li className="active">
                        <Link to="/">
                        Home</Link>
                      </li>
                      <li>
                        <Link to="/products">
                        Shop</Link>
                      </li>
                      <li>
                        <Link to="/about">
                        About</Link>
                      </li>
                      <li>
                        <Link to="/blog">
                        Blog</Link>
                      </li>
                      <li>
                        <Link to="/contact">
                        Contact</Link>
                      </li>
                      <li>
                        <Link to="/register">
                        Register</Link>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
            <div className="col-xl-2 col-lg-2 col-md-2 col-sm-2">
              <li>
                <Link to="/login" className="buy">
                Login</Link>
              </li>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
