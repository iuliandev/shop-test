<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!-- basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- mobile metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="initial-scale=1, maximum-scale=1">
        <!-- site metas -->
        <title>Lighten</title>
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- bootstrap css -->
        <link rel="stylesheet" href="{{ asset('lighten/css/bootstrap.min.css') }}">
        <!-- style css -->
        <link rel="stylesheet" href="{{ asset('lighten/css/style.css') }}">
        <!-- Responsive-->
        <link rel="stylesheet" href="{{ asset('lighten/responsive.css') }}">
        <!-- favicon -->
        <link rel="icon" href="{{ asset('lighten/images/fevicon.png') }}" type="image/gif" />
        <!-- Scrollbar Custom CSS -->
        <link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.min.css') }}">
        <!-- Tweaks for older IEs-->
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <div id="root"></div>

        <script src="{{ asset('js/app.js') }}"></script>

        <!-- Javascript files-->
        <script src="{{ asset('lighten/js/jquery.min.js') }}"></script>
        <script src="{{ asset('lighten/js/popper.min.js') }}"></script>
        <script src="{{ asset('lighten/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('lighten/js/jquery-3.0.0.min.js') }}"></script>
        <script src="{{ asset('lighten/js/plugin.js') }}"></script>

        <!-- sidebar -->
        <script src="{{ asset('lighten/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
        <script src="{{ asset('lighten/js/custom.js') }}"></script>
        <script src="https:cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
        <script>
           $(document).ready(function(){
           $(".fancybox").fancybox({
           openEffect: "none",
           closeEffect: "none"
           });

           $(".zoom").hover(function(){

           $(this).addClass('transition');
           }, function(){

           $(this).removeClass('transition');
           });
           });

        </script>
    </body>
</html>
