<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'quantity',
        'image',
        'status',
    ];

    /**
     * The categories that belong to the product.
     */
    public function categories()
    {
        return $this->belongsToMany('\App\Category', 'product_category');
    }

    /**
     * Attach product to many categories.
     */
    public function add_categories($category_ids)
    {
        return $this->categories()->attach($category_ids);
    }

    /**
     * Update product categories.
     */
    public function update_categories($category_ids)
    {
        return $this->categories()->sync($category_ids);
    }
}
