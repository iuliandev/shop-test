<?php

namespace App\Repositories;

use App\Product;

class ProductRepository
{
    private $product;

    /**
     * Constructor
     *
     * @param Product $product Product entity
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     *  Get Products
     *
     * @param integer $perPage
     *
     * @return Product
     */
    public function get($perPage) {
        $products = $this->product->paginate($perPage);

        return $products;
    }

    /**
     *  Get Product by id
     *
     * @param integer $id
     *
     * @return Product
     */
    public function getById($id) {
        $product = $this->product->find($id);

        return $product;
    }

    /**
     *  Create Product
     *
     * @param array $input
     *
     * @return Product
     */
    public function create($input) {
        $product = $this->product->create($input);

        return $product;
    }

    /**
     *  Attach Product to Categories
     *
     * @param Product $product
     * @param array $categories
     *
     * @return Product
     */
    public function attachToCategories($product, $categories) {
        $product->add_categories($categories);
    }

    /**
     *  Update Product Categories
     *
     * @param Product $product
     * @param array $categories
     *
     * @return Product
     */
    public function updateProductCategories($product, $categories) {
        $product->update_categories($categories);
    }
}
