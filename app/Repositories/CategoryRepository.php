<?php

namespace App\Repositories;

use App\Category;

class CategoryRepository
{
    private $category;

    /**
     * Constructor
     *
     * @param Category $category Category entity
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     *  Create Category
     *
     * @param array $input
     *
     * @return Category
     */
    public function create($input) {
        $category = $this->category->create($input);

        return $category;
    }

    /**
     *  Get Categories
     *
     * @param integer $perPage
     *
     * @return Category
     */
    public function get($perPage) {
        $categories = $this->category->paginate($perPage);

        return $categories;
    }

    /**
     *  Get Category by id
     *
     * @param integer $id
     *
     * @return Category
     */
    public function getById($id) {
        $category = $this->category->find($id);

        return $category;
    }

    /**
     *  Update Category parent_id
     *
     * @param integer $id
     */
    public function updateChildsParentId($id) {
        $this->category->where('parent_id', $id)
                       ->update(array('parent_id' => null));
    }

    /**
     *  Delete Category
     *
     * @param Category $category
     */
    public function delete($category) {
        $category = $category->delete();

        return $category;
    }
}
