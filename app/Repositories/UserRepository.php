<?php

namespace App\Repositories;

use App\User;

class UserRepository
{
    private $user;

    /**
     * Constructor
     *
     * @param User $user User entity
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     *  Create User
     *
     * @param array $input
     *
     * @return User
     */
    public function create($input) {
        $user = $this->user->create($input);

        return $user;
    }

    /**
     *  Update User
     *
     * @param Request $request
     */
    public function update($request) {
        auth()->user()->updateFields($request);
    }

    /**
     *  Get User by email
     *
     * @param string $email
     *
     * @return User
     */
    public function getByEmail($email) {
        $user = $this->user->where('email', $email)
                           ->first();

        return $user;
    }
}
