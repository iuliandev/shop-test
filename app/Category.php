<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'parent_id',
    ];

    /**
     * Get subcategories
     *
     */
    public function subcategories()
    {
        return $this->hasMany('\App\Category', 'parent_id','id');
    }

    /**
     * Get parent category
     *
     */
    public function parent_category()
    {
        return $this->belongsTo('\App\Category', 'parent_id');
    }

    /**
     * The products that belong to the category.
     */
    public function products()
    {
        return $this->belongsToMany('\App\Product', 'product_category');
    }
}
