<?php

function debugInfo($e)
{
    $message = $e->getMessage() . ' in file ' . $e->getFile() . ' on line ' . $e->getLine();

    return $message;
}
