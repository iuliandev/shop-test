<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\ProductCollection;
use App\Repositories\CategoryRepository;

class CategoryProductsController extends AppController
{
    /**
     * Category repository
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Products per page
     *
     * @var integer
     */
    private $productsPerPage = 10;

    /**
     * Constructor
     *
     * @param CategoryRepository $categoryRepository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display category with products
     *
     * @param  int  $categoryId
     * @return ProductCollection
     */
    public function show($categoryId)
    {
        try {
            $category = $this->categoryRepository->getById($categoryId);

            if(!$category) {
                return jsonResponse('error', 401, [
                    'message' => 'Category not found.'
                ]);
            }

            $categoryProducts = $category->products()->paginate($this->productsPerPage);

            return jsonResponse('success', 200, new ProductCollection($categoryProducts));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
