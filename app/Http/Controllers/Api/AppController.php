<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AppController extends Controller
{

    public $defaultErrorMessage = 'Something went wrong! Please try again or contact us to report it.';

}
