<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\AppController;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;

class UserController extends AppController
{
    /**
     * User repository
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Constructor
     *
     * @param UserRepository $userRepository User repository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @return response
     */
    public function show(Request $request)
    {
        try {
            $user = $request->user();

            return jsonResponse('success', 200, new UserResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\UserUpdateRequest  $request
     * @return response
     */
    public function update(UserUpdateRequest $request)
    {
        try {
            $user = $request->user();

            if(!$user) {
                return jsonResponse('error', 401, [
                    'message' => 'User not found.'
                ]);
            }

            $this->userRepository->update($request);

            return jsonResponse('success', 200, new UserResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
