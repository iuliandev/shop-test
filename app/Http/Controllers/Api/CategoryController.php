<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Requests\CategoryStoreRequest;
use App\Http\Resources\Category as CategoryResource;
use App\Http\Resources\CategoryCollection;
use App\Repositories\CategoryRepository;

class CategoryController extends AppController
{
    /**
     * Category repository
     *
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * Categories per page
     *
     * @var integer
     */
    private $categoriesPerPage = 10;

    /**
     * Constructor
     *
     * @param CategoryRepository $categoryRepository Category repository
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return CategoryCollection
     */
    public function index()
    {
        try {
            $categories = $this->categoryRepository->get($this->categoriesPerPage);

            return jsonResponse('success', 200, new CategoryCollection($categories));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param App\Http\Requests\CategoryStoreRequest $request
     * @return CategoryResource
     */
    public function store(CategoryStoreRequest $request)
    {
        try {
            $input = $request->all();
            $category = $this->categoryRepository->create($input);

            return jsonResponse('success', 201, new CategoryResource($category));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return CategoryResource
     */
    public function show($id)
    {
        try {
            $category = $this->categoryRepository->getById($id);

            if(!$category) {
                return jsonResponse('error', 401, [
                    'message' => 'Category not found.'
                ]);
            }

            return jsonResponse('success', 200, new CategoryResource($category));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\CategoryStoreRequest  $request
     * @param  int  $id
     * @return CategoryResource
     */
    public function update(CategoryStoreRequest $request, $id)
    {
        try {
            $category = $this->categoryRepository->getById($id);

            if(!$category) {
                return jsonResponse('error', 401, [
                    'message' => 'Category not found.'
                ]);
            }

            $input = $request->all();
            $category->update($input);

            return jsonResponse('success', 200, new CategoryResource($category));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return string
     */
    public function destroy($id)
    {
        try {
            $category = $this->categoryRepository->getById($id);

            if(!$category) {
                return jsonResponse('error', 401, [
                    'message' => 'Category not found.'
                ]);
            }

            $this->categoryRepository->updateChildsParentId($id);
            $category->delete();

            return jsonResponse('success', 200, [
                'message' => 'Category successfully was removed.'
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
