<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AppController;
use App\Http\Resources\Product as ProductResource;
use App\Http\Resources\ProductCollection;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Repositories\ProductRepository;

class ProductController extends AppController
{
    /**
     * Product repository
     *
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Products per page
     *
     * @var integer
     */
    private $productsPerPage = 10;

    /**
     * Constructor
     *
     * @param ProductRepository $productRepository Product repository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return ProductCollection
     */
    public function index()
    {
        try {
            $products = $this->productRepository->get($this->productsPerPage);

            return jsonResponse('success', 200, new ProductCollection($products));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\ProductCreateRequest  $request
     * @return ProductResource
     */
    public function store(ProductCreateRequest $request)
    {
        try {
            $input = $request->all();
            $categories = $request->get('category');
            $product = $this->productRepository->create($input);
            $this->productRepository->attachToCategories($product, $categories);

            return jsonResponse('success', 201, new ProductResource($product));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return ProductResource
     */
    public function show($id)
    {
        try {
            $product = $this->productRepository->getById($id);

            if(!$product) {
                return jsonResponse('error', 401, [
                    'message' => 'Product not found.'
                ]);
            }

            return jsonResponse('success', 200, new ProductResource($product));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ProductUpdateRequest  $request
     * @param  int  $id
     * @return ProductResource
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        try {
            $product = $this->productRepository->getById($id);

            if(!$product) {
                return jsonResponse('error', 401, [
                    'message' => 'Product not found.'
                ]);
            }

            $input = $request->all();
            $categories = $request->get('category');

            $product->update($input);
            $this->productRepository->updateProductCategories($product, $categories);

            return jsonResponse('success', 200, new ProductResource($product));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = $this->productRepository->getById($id);

            if(!$product) {
                return jsonResponse('error', 401, [
                    'message' => 'Product not found.'
                ]);
            }

            $product->delete();

            return jsonResponse('success', 200, [
                'message' => 'Product successfully was removed.'
            ]);
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
