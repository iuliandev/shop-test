<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Resources\UserAuthResource;
use App\Http\Controllers\Api\AppController;
use App\Repositories\UserRepository;

class AuthController extends AppController
{

    /**
     * User repository
     *
     * @var UserRepository
     */
    private $userRepository;

    /**
     * Constructor
     *
     * @param UserRepository $userRepository User repository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Create new User
     *
     * @param UserRegisterRequest $request
     *
     * @return array
     */
    public function register(UserRegisterRequest $request)
    {
        try {
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);

            $user = $this->userRepository->create($input);

            return jsonResponse('success', 200, new UserAuthResource($user));
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Login User
     *
     * @param UserLoginRequest $request
     *
     * @return array
     */
    public function login(UserLoginRequest $request)
    {
        try {
            $user = $this->userRepository->getByEmail($request->email);

            if(!$user) {
                return jsonResponse('error', 401, [
                    'message' => 'Account with this email does not exist.'
                ]);
            }

            if(Hash::check($request->password, $user->password)) {
                return jsonResponse('success', 200, new UserAuthResource($user));
            } else {
                return jsonResponse('error', 401, [
                    'password' => 'Your password is incorrect.'
                ]);
            }
        } catch(\Exception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }

    /**
     * Log out User
     *
     * @return array
     */
    public function logout()
    {
        try {
            if (auth()->check()) {
                auth()->user()->OauthAcessToken()->delete();

                return jsonResponse('success', 200, [
                    'message' => 'You was logout.'
                ]);
            }

            return jsonResponse('error', 400, [
                'message' => 'You are logout.'
            ]);
        } catch(\Ecception $e) {
            \Log::info(debugInfo($e));

            return jsonResponse('error', 500, $this->defaultErrorMessage);
        }
    }
}
