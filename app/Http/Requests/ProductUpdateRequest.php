<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProductUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required|string|max:255',
            'price'      => "required|regex:/^\d+(\.\d{1,2})?$/",
            'quantity'   => 'required|integer',
            'image'      => 'nullable|string|max:255',
            'status'     => 'required'
        ];
    }
}
