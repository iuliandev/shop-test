<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            'price'       => $this->price,
            'quantity'    => $this->quantity,
            'image'       => $this->image,
            'status'      => (bool) $this->status,
            'created_at'  => $this->created_at->diffForHumans(),
            'url'         => '/products/show/'.$this->id
        ];
    }
}
