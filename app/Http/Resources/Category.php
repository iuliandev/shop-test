<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CategoryCollection;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $parent_categroy = $this->parent_category ? array(
                                  'id'   => $this->parent_category->id,
                                  'name' => $this->parent_category->name,
                              ) : array();

        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'products_count'   => $this->products->count(),
            'created_at'       => $this->created_at->diffForHumans(),
            'url'              => '/categories/show/'.$this->id,
            'parent_category'  => $parent_categroy,
            'subcategories'    => new CategoryCollection($this->subcategories)
        ];
    }
}
