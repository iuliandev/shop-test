<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => ucfirst($faker->word),
        'price' => $faker->numberBetween(5, 9999),
        'quantity' => $faker->numberBetween(0, 9999),
        'image' => $faker->imageUrl(),
        'status' => $faker->numberBetween(0, 1),
    ];
});
