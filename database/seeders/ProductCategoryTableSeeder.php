<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ProductCategoryTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('product_category')->delete();

        \DB::table('product_category')->insert(array ( ));


    }
}
