<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Lucius Beahan',
                'email' => 'boyle.alisha@example.org',
                'email_verified_at' => '2019-11-10 15:09:24',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => '1LfhadA8LE',
                'created_at' => '2019-11-10 15:09:24',
                'updated_at' => '2019-11-10 15:09:24',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Lauriane Dare',
                'email' => 'jesus43@example.net',
                'email_verified_at' => '2019-11-10 15:09:24',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => '2YPZ2unuDk',
                'created_at' => '2019-11-10 15:09:24',
                'updated_at' => '2019-11-10 15:09:24',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Scotty Reinger',
                'email' => 'reichel.era@example.org',
                'email_verified_at' => '2019-11-10 15:09:24',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'j5AS3ADASy',
                'created_at' => '2019-11-10 15:09:24',
                'updated_at' => '2019-11-10 15:09:24',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'Shaina Boyle II',
                'email' => 'zemlak.johanna@example.net',
                'email_verified_at' => '2019-11-10 15:09:24',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'wNKVbqq0ep',
                'created_at' => '2019-11-10 15:09:24',
                'updated_at' => '2019-11-10 15:09:24',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'Margaret Hodkiewicz',
                'email' => 'fidel56@example.org',
                'email_verified_at' => '2019-11-10 15:09:24',
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                'remember_token' => 'PCf7Wx1eTL',
                'created_at' => '2019-11-10 15:09:24',
                'updated_at' => '2019-11-10 15:09:24',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'Hercule',
                'email' => 'test@gmail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$PlAJya55d47l1yMmaOH0yetdNpnrcix8f6eBJxgpc.DXAdR7molki',
                'remember_token' => NULL,
                'created_at' => '2019-11-10 14:38:18',
                'updated_at' => '2019-11-10 14:38:18',
            ),
        ));


    }
}
