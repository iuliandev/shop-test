<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('oauth_clients')->delete();

        \DB::table('oauth_clients')->insert(array (
            0 =>
            array (
                'id' => 1,
                'user_id' => NULL,
                'name' => 'Laravel Personal Access Client',
                'secret' => '7OvmHfJ9oo8zYo9QTVkgFso3WFjwN91a1Iam6vCF',
                'redirect' => 'http://localhost',
                'personal_access_client' => 1,
                'password_client' => 0,
                'revoked' => 0,
                'created_at' => '2019-11-10 13:49:02',
                'updated_at' => '2019-11-10 13:49:02',
            ),
            1 =>
            array (
                'id' => 2,
                'user_id' => NULL,
                'name' => 'Laravel Password Grant Client',
                'secret' => '57c2xmiMSXpmXFtZFGKhSfzqVvSdOayIcFMXp1VM',
                'redirect' => 'http://localhost',
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => '2019-11-10 13:49:02',
                'updated_at' => '2019-11-10 13:49:02',
            ),
        ));


    }
}
