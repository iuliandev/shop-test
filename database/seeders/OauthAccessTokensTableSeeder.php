<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OauthAccessTokensTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('oauth_access_tokens')->delete();

        \DB::table('oauth_access_tokens')->insert(array (
            0 =>
            array (
                'id' => 'e6510096a3f61ee6b55a05d395cc555705fd3bba3cb120b0f9d6af63ecdd6fdb92cb7f9634738d29',
                'user_id' => 6,
                'client_id' => 1,
                'name' => 'AppName',
                'scopes' => '[]',
                'revoked' => 0,
                'created_at' => '2019-11-10 19:30:27',
                'updated_at' => '2019-11-10 19:30:27',
                'expires_at' => '2020-11-10 19:30:27',
            ),
        ));


    }
}
